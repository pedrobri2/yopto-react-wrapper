import React, { FC, useMemo } from "react";

import decodeProductId from "../../utils/decodeProductId";
import { useYotpoRefresh } from "../../hooks/useYotpoRefresh";

export type YotpoReviewsProps = {
  productId: string;
  price: string;
  currency: string;
  urlPath: string;
  image: string;
  name: string;
  description: string;
};

/**
 * Adds a div to the DOM for Yotpo to embed reviews
 */
const YotpoReviews: FC<YotpoReviewsProps> = ({
  productId = "",
  price = "",
  currency = "",
  urlPath = "",
  image = "",
  name = "",
  description = "",
}) => {
  useYotpoRefresh();

  return (
    <div
      className="yotpo yotpo-main-widget"
      data-product-id={productId}
      data-price={price}
      data-currency={currency}
      data-name={name}
      data-url={`${window.location.origin}${urlPath}`}
      data-image-url={image}
      data-description={description}
    />
  );
};

export default YotpoReviews;
