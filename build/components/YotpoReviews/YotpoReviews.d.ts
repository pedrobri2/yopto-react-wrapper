import { FC } from "react";
export declare type YotpoReviewsProps = {
    productId: string;
    price: string;
    currency: string;
    urlPath: string;
    image: string;
    name: string;
    description: string;
};
/**
 * Adds a div to the DOM for Yotpo to embed reviews
 */
declare const YotpoReviews: FC<YotpoReviewsProps>;
export default YotpoReviews;
